<?php

namespace Drupal\score_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'score_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "score_field_widget",
 *   label = @Translation("Score field widget"),
 *   field_types = {
 *     "score_field_type"
 *   }
 * )
 */
class ScoreFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size_team' => 60,
      'size_score' => 7,
      'placeholder_1' => 'Team 1',
      'placeholder_2' => 'Team 2',
      'placeholder_3' => 'Score 1',
      'placeholder_4' => 'Score 2',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size_team'] = [
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size_team'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $elements['placeholder_1'] = [
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder_1'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size_team')]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder_1')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['team_1'] = $element + [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#size' => $this->getSetting('size_team'),
      '#placeholder' => $this->getSetting('placeholder_1'),
      '#maxlength' => $this->getFieldSetting('max_length'),
    ];

    $element['team_2'] = $element + [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#size' => $this->getSetting('size_team'),
      '#placeholder' => $this->getSetting('placeholder_2'),
      '#maxlength' => $this->getFieldSetting('max_length'),
    ];

    $element['score_1'] = $element + [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#size' => $this->getSetting('size_score'),
      '#placeholder' => $this->getSetting('placeholder_3'),
      '#maxlength' => $this->getFieldSetting('max_length_score'),
    ];

    $element['score_2'] = $element + [
      '#type' => 'textfield',
      '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
      '#size' => $this->getSetting('size_score'),
      '#placeholder' => $this->getSetting('placeholder_4'),
      '#maxlength' => $this->getFieldSetting('max_length_score'),
    ];

    return $element;
  }

}
